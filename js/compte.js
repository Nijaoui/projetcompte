class Compte {
    /** 
     * @param {string} prenomClient
     * @param {string} nomClient 
     * @param {number} numeroClient 
     * @param {date} dateCreationCompte
     * @param {number} solde  
       
     
     */
    constructor(prenomClient, nomClient, numeroClient, dateCreationCompte, solde) {
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.numeroClient = numeroClient;
        this.dateCreationCompte = dateCreationCompte;
        this.solde = solde;
    }
    retrait(montant) {
        this.solde = this.solde - montant; //this.solde = this.solde - montant
    }
};



